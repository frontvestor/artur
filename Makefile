# Makefile
#

.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

clean: build rm run ## Build, remove and run container

build: ## Build Docker image
	docker build --tag frontvestor/artur:latest .

run: ## Run container
	docker run --name artur --detach --publish 80:80 frontvestor/artur

rm: ## Remove container
	docker rm --force artur

login: ## Login to container
	docker exec -it artur /bin/sh

logs: ## Follow logs
	docker logs --follow artur

browse: ## Browse the app
	open http://localhost
