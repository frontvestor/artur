from setuptools import setup

setup(
    name='artur',
    packages=['artur'],
    include_package_data=True,
    install_requires=[
        'flask',
        'freezegun',
    ],
)
