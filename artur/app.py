from flask import Flask

from artur.app.names import get_last_name

app = Flask(__name__)


@app.route('/')
def index():
    return "See what I have in: " + "<br><br>" \
           "/hello-world" + "<br>"\
           "/hello/artur" + "<br>"\
           "/hello/pablo"


@app.route('/hello-world')
def hello_world():
    return {'Hello': "World!"}


@app.route('/hello/<first_name>')
def hello(first_name):
    last_name = get_last_name(first_name)
    return {'Hello': last_name}


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
