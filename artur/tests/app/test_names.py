import unittest

from artur.app import names


class AppNamesTest(unittest.TestCase):

    def test_get_last_name_should_return_arturs_last_name_if_first_name_is_artur(self):
        first_name = 'artur'
        expected = 'Wätterbäck Höppner'
        result = names.get_last_name(first_name)
        self.assertEqual(result, expected)

    def test_get_last_name_should_return_default_value_if_first_name_does_not_match(self):
        first_name = 'foo'
        expected = 'Unknown user'
        result = names.get_last_name(first_name)
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
