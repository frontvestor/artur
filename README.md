# Artur

A Hello World REST app: http://104.196.205.76 

## Endpoints

- /
- /hello-world
- /hello/<first_name>

## Local pre-requisites:

Docker is needed in order to build the Docker container locally and run it.

Docker can be installed from e.g: https://docs.docker.com/docker-for-mac/install/ (short-link: [Docker.dmg](https://download.docker.com/mac/stable/Docker.dmg))

## Run locally:

Running `make` will show all available make targets, which are defined in the `Makefile`.

### 1. Build Docker image

The first step is to build the Docker image:

```
make build
```

This will run the tests and create the image.

### 2. Run container

The second step is to run a container based on the Docker image:

```
make run
```

If you already have a running container, make sure to remove it first with:

```
make rm
```

### 3. Browse

With a running container, it is now possible to browse the site at http://localhost:

```
make browse
```
