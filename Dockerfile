FROM python:3.7-alpine


##
# Set Swedish time zon
##
RUN apk add tzdata && \
    ln -sf /usr/share/zoneinfo/Europe/Stockholm /etc/localtime


##
# Install requirements
##
COPY setup.py /app/setup.py
COPY artur /app/artur/__init__.py
WORKDIR /app
RUN pip install --editable .


##
# Copy source files
##
COPY . /app


##
# Run tests
##
RUN python artur/tests/app/test_names.py


##
# Run app
##
CMD ["python", "artur/app.py"]
